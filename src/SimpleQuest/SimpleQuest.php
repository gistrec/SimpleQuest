<?php
/* Пони правят миром     *
 * Автор плагина gistrec *
 * vk.com/AreaOfDefect   */
namespace SimpleQuest;

use pocketmine\plugin\PluginBase;

use pocketmine\event\Listener;
use pocketmine\event\block\BlockPlaceEvent;
use pocketmine\event\block\BlockBreakEvent;
use pocketmine\event\entity\EntityDamageByEntityEvent;
use pocketmine\event\entity\EntityDamageEvent;

use pocketmine\entity\Entity;
use pocketmine\entity\Human;

use pocketmine\Player;

use pocketmine\inventory\PlayerInventory;

use pocketmine\command\Command;
use pocketmine\command\CommandSender;
use pocketmine\command\ConsoleCommandSender;

use pocketmine\utils\Config;

use pocketmine\item\Item;

use pocketmine\nbt\tag\ByteTag;
use pocketmine\nbt\tag\CompoundTag;
use pocketmine\nbt\tag\DoubleTag;
use pocketmine\nbt\tag\ListTag;
use pocketmine\nbt\tag\FloatTag;
use pocketmine\nbt\tag\IntTag;
use pocketmine\nbt\tag\ShortTag;
use pocketmine\nbt\tag\StringTag;


class SimpleQuest extends PluginBase implements Listener{

    const VERSION = "1.0.0";

    public $config;
    public $data;

    public $economy;

    public function onEnable(){
        @mkdir($this->getDataFolder());
        if (!file_exists($this->getDataFolder().'/config.yml'))  $this->saveResource("config.yml");
        if (!file_exists($this->getDataFolder().'/data.yml'))  $this->saveResource("data.yml");
        $this->config = (new Config($this->getDataFolder().'config.yml', Config::YAML))->getAll();
        $this->data = (new Config($this->getDataFolder().'data.yml', Config::YAML))->getAll();
        $this->economy = $this->getServer()->getPluginManager()->getPlugin("EconomyAPI");
        $this->getServer()->getPluginManager()->registerEvents($this, $this);
        $this->getServer()->getLogger("§dSimpleQuest §bby gistrec §aзагружен!");
        $this->checkEconomy();
        $this->checkUpdate();
    }

    public function onDisable() {
        $save = new Config($this->getDataFolder().'data.yml', Config::YAML);
        $save->setAll($this->data);
        $save->save();
    }

    public function checkEconomy() {
        if ($this->economy == null) {
            $this->getServer()->getLogger()->error("§cДля работы §dSimpleQuest §cнужен плагин §dEconomyAPI");
        }
    }

    public function checkUpdate() {
        $query = "http://gistrec.ru/simplequest/" .self::VERSION;
        @$msg = file_get_contents($query);
        # Вывод сообщения о новой версии :)
        if ($msg != self::VERSION) $this->getServer()->getLogger()->warning($msg);
    }

    public function onCommand(CommandSender $sender, Command $cmd, $label, array $args) {
        if ($cmd != 'quest') return;
        if (!$sender instanceof Player) return ($sender->sendMessage("§aИспользуйте эту команду в игре"));
        if (!isset($args[0])) $args[0] = null;
        switch (strtolower($args[0])) {
            case 'info':
                if (isset($this->data[strtolower($sender->getName())])) {
                    $data = $this->data[strtolower($sender->getName())];
                    $data = explode(":", $data);
                    $msg = $this->config['task'][$data[2]]["info_msg"];
                    $msg = str_replace("{now}", $data[0], $msg);
                    $msg = str_replace("{count}", $data[1], $msg);
                    $msg = str_replace("{money}", $data[3], $msg);
                    $msg = str_replace("{left}", $data[1] - $data[0], $msg);
                    $sender->sendMessage($msg);
                }else $sender->sendMessage($this->config["msg_no_quest"]);
                break;
            case 'cancel':
                if (isset($this->data[strtolower($sender->getName())])) {
                    unset($this->data[strtolower($sender->getName())]);
                    $sender->sendMessage($this->config["msg_quest_cancel"]);
                }else $sender->sendMessage($this->config["msg_no_quest"]);
                break;
            case 'create':
                if (!$sender->isOp()) return ($sender->sendMessage($this->config["msg_create_no_rule"]));
                if (!isset($args[1])) return ($sender->sendMessage($this->config["msg_no_name"]));
                else {
                    $this->createEntity($sender->getSkinData(), $sender->getSkinId(), $args[1], $sender->getInventory(), $sender->getYaw(), $sender->getPitch(), $sender->getX(), $sender->getY(), $sender->getZ(), $sender->getLevel());
                    $sender->sendMessage($this->config["msg_create"]);
                }
                break;
            case 'help':
            default:
                $sender->sendMessage("§b/quest info §aинформация о текущем квесте\n".
                                     "§b/quest cancel §aотменить текущий квест\n".
                                     "§b/quest help §aполучить информацию по плагину\n");
                break;
        }
    }

    public function onPlace(BlockPlaceEvent $event) {
        $playerName = strtolower($event->getPlayer()->getName());
        if (isset($this->data[$playerName])) {
            $data = $this->data[$playerName];
            $data = explode(":", $data);
            if ($data[2] != "place") return;
            if ($data[0] < $data[1]) $data[0]++;
            $data = implode(":", $data);
            $this->data[$playerName] = $data;
        }
    }

    public function omBreak(BlockBreakEvent $event) {
        $playerName = strtolower($event->getPlayer()->getName());
        if (isset($this->data[$playerName])) {
            $data = $this->data[$playerName];
            $data = explode(":", $data);
            if ($data[2] != "break") return;
            if ($data[0] < $data[1]) $data[0]++;
            $data = implode(":", $data);
            $this->data[$playerName] = $data;
        }
    }

    public function onDamage(EntityDamageEvent $event){
        if ($event instanceof EntityDamageByEntityEvent && $event->getDamager() instanceof Player){
            if ($event->getEntity()->namedtag["SimpleQuest"] != NULL && $event->getEntity() instanceof Human){
                $event->setCancelled();
                if ($event->getDamager()->isOp() && $event->getDamager()->getInventory()->getItemInHand()->getId() == $this->config['kill_item_id']) {
                    $event->getDamager()->sendMessage($this->config['msg_delete']);
                    $event->getEntity()->close();
                    return;
                }
                $playerName = strtolower($event->getDamager()->getName());
                if (isset($this->data[$playerName])) {
                    $data = $this->data[$playerName];
                    $data = explode(":", $data);
                    if ($data[0] >= $data[1]) {
                        $msg = $this->config['task'][$data[2]]["finish_msg"];
                        $msg = str_replace("{now}", $data[0], $msg);
                        $msg = str_replace("{count}", $data[1], $msg);
                        $msg = str_replace("{money}", $data[3], $msg);
                        $msg = str_replace("{left}", $data[1] - $data[0], $msg);
                        $event->getDamager()->sendMessage($msg);
                        unset($this->data[$playerName]);
                        // REWARD
                        if ($this->economy != null) $this->economy->addMoney($event->getDamager(), $data[3]);
                    }else {
                        $msg = $this->config['task'][$data[2]]["info_msg"];
                        $msg = str_replace("{now}", $data[0], $msg);
                        $msg = str_replace("{count}", $data[1], $msg);
                        $msg = str_replace("{money}", $data[3], $msg);
                        $msg = str_replace("{left}", $data[1] - $data[0], $msg);
                        $event->getDamager()->sendMessage($msg);
                    }
                }else {
                    $data = false;
                    while (!$data) {
                        $data = $this->config['task'][$type = array_rand($this->config['task'])];
                        if ($data['quests'] != []) $data = $data['quests'][array_rand($data['quests'])];
                        else $data = false;
                    }
                    $data = explode(":", $data);
                    $msg = $this->config['task'][$type]["start_msg"];
                    $msg = str_replace("{now}", "0", $msg);
                    $msg = str_replace("{count}", $data[0], $msg);
                    $msg = str_replace("{money}", $data[1], $msg);
                    $msg = str_replace("{left}", $data[0], $msg);
                    $event->getDamager()->sendMessage($msg);
                    $data = "0:" . $data[0] . ":" . $type . ":" . $data[1];
                    $this->data[$playerName] = $data;
                }
            // Относится к EntityDamageByEntityEvent
            }elseif (($event->getEntity()->getHealth() - $event->getFinalDamage() <= 0) && $event->getEntity() instanceof Player) {
                $playerName = strtolower($event->getDamager()->getName());
                if (isset($this->data[$playerName])) {
                    $data = $this->data[$playerName];
                    $data = explode(":", $data);
                    if ($data[2] != "kill") return;
                    if ($data[0] < $data[1]) $data[0]++;
                    $data = implode(":", $data);
                    $this->data[$playerName] = $data;
                }
            }
        }
    }

    public function createEntity($skin, $skinId, $name, $inventory, $yaw, $pitch, $x, $y, $z, $level){
        $nbt = new CompoundTag;
        $nbt->Pos = new ListTag("Pos", [
            new DoubleTag("", $x),
            new DoubleTag("", $y),
            new DoubleTag("", $z)
        ]);
        $nbt->Rotation = new ListTag("Rotation", [
            new FloatTag("", $yaw),
            new FloatTag("", $pitch)
        ]);
        $nbt->Health = new ShortTag("Health", 20);
        $nbt->CustomName = new StringTag("CustomName", $name);
        $nbt->SimpleQuest = new StringTag("SimpleQuest", "true");
        //$nbt->CustomNameVisible = new ByteTag("CustomNameVisible", 1);
        $nbt->CustomNameVisible = new StringTag("CustomNameVisible", "true");
        $nbt->Inventory = new ListTag("Inventory", $inventory);
        $nbt->Skin = new CompoundTag("Skin", ["Data" => new StringTag("Data", $skin), "Name" => new StringTag("Name", $skinId)]);
        $entity = Entity::createEntity("Human", $level, $nbt);
        $entity->setNameTagVisible(true);
        $entity->setNameTagAlwaysVisible(true);
        $entity->saveNBT();
        $inv = $entity->getInventory();
        $inv->setHelmet($inventory->getHelmet());
        $inv->setChestplate($inventory->getChestplate());
        $inv->setLeggings($inventory->getLeggings());
        $inv->setBoots($inventory->getBoots());
        $inv->setHeldItemSlot($inventory->getHeldItemSlot());
        $inv->setItemInHand($inventory->getItemInHand());
        $entity->spawnToAll();
    }
}